import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Welcome from './Components/Welcome';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Switch>
        <Route exact path="/Welcome" component={Welcome} />
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
