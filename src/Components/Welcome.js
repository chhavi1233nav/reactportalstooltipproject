import React, { Component } from 'react';

import Homepage from './Homepage';
import TooltipManager from './Tooltips/TooltipManager';

import './Welcome.css';

class Welcome extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentTooltipName: null,
        };
    }

    firstAction = () => {
        setTimeout(() => {
            this.setState({ currentTooltipName: 'event' });
            document.querySelector('#event-message').classList.remove('hidden');
        }, 500);
    }

    componentDidMount() {
        this.setState({ currentTooltipName: 'welcome' });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.currentTooltipName === 'welcome' && this.state.currentTooltipName === null) {
            return this.setState({ currentTooltipName: 'first_action' });
        }
    }

    onTooltipClose = () => {
        this.setState({
            currentTooltipName: null,
        });
    }

    render() {
        return (
            <div className="app">
                <div className="welcome-msg">
                    <div id="tm-welcome-tooltip" className="tooltip-target">
                        Welcome to the Tooltip Manager Demo
                    </div>
                </div>
                <div id="root"></div>
                <div id="event-message" className="event-msg  hidden">
                    <div id="tm-event-tooltip" className="tooltip-target">
                        <span>This is the test app to show how we can use React Portals to create a Tooltip Manager.</span>
                    </div>
                </div>
                <Homepage onClick={this.firstAction} />
                <TooltipManager
                    currentTooltip={this.state.currentTooltipName}
                    onTooltipClose={this.onTooltipClose}
                />
            </div>
        );
    }
}

export default Welcome
    ;